#pragma once
#include "QueueNode.h"

class Queue
{
public:
	Queue();
	bool isEmpty();
	bool enQueue(string newItem);
	string deQueue();
	void printQueue();
	void setHead(QueueNode* newPtr);
	void setTail(QueueNode* newPtr);
	QueueNode* getHead();
	QueueNode* getTail();
private:
	QueueNode* mHead;
	QueueNode* mTail;
};