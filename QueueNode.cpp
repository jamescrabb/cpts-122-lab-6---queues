#include "QueueNode.h"

QueueNode::QueueNode(string newItem)
{
	mItem = newItem;
	mPtr = nullptr;
}

string QueueNode::getItem()
{
	return mItem;
}

QueueNode * QueueNode::getPtr()
{
	return mPtr;
}

void QueueNode::setPtr(QueueNode * newPtr)
{
	mPtr = newPtr;
}
