#pragma once
#include <iostream>
#include <string>

using namespace std;

class QueueNode
{
public:
	QueueNode(string newItem);
	string getItem();
	QueueNode* getPtr();
	void setPtr(QueueNode* newPtr);
private:
	string mItem;
	QueueNode* mPtr;
};