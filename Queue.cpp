#include "Queue.h"

Queue::Queue()
{
	setHead(nullptr);
	setTail(nullptr);
}

bool Queue::isEmpty()
{
	return getHead() == nullptr;
}

bool Queue::enQueue(string newItem)
{
	bool validate = false;
	QueueNode* pTemp = new QueueNode(newItem);

	if (isEmpty())
	{
		setHead(pTemp);
		setTail(pTemp);
		validate = true;
	}
	else
	{
		mTail->setPtr(pTemp);
		setTail(pTemp);
	}

	return validate;
}

string Queue::deQueue()
{
	QueueNode* pTemp = mHead;
	setHead(mHead->getPtr());
	string str = pTemp->getItem();
	delete pTemp;
	return str;
}

void Queue::printQueue()
{
	while (mHead)
	{
		cout << deQueue() << endl;
	}
}

void Queue::setHead(QueueNode * newPtr)
{
	mHead = newPtr;
}

void Queue::setTail(QueueNode * newPtr)
{
	mTail = newPtr;
}

QueueNode * Queue::getHead()
{
	return mHead;
}

QueueNode * Queue::getTail()
{
	return mTail;
}
